import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {GeneralUtil} from "../../util/general-util";
import {Observable, throwError} from "rxjs";
import {LoginDto} from "../../dto/login-dto";
import {GeneralEnum} from "../../constant/general-enum.enum";
import {catchError} from "rxjs/operators";
import {RegisterDto} from "../../dto/register-dto";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public static userDetailsEmail = 'USER-DETAILS-EMAIL';
  public static userDetailsFullName = 'USER-DETAILS-FULL-NAME';
  public static userDetailsRole = 'USER-DETAILS-ROLE';
  public static userDetailsToken = 'USER-DETAILS-TOKEN';


  constructor(private httpClient: HttpClient) {
  }

  // Login user
  loginUser(loginDto: LoginDto): Observable<any> {

    const url = GeneralEnum.baseUrl + '/login';

    console.log(loginDto);

    return this.httpClient.post<any>(url, loginDto)
      .pipe(catchError(this.errorHandler));
  }

  registerUser(dto: RegisterDto): Observable<any> {

    const url = GeneralEnum.baseUrl + '/register';

    return this.httpClient.post<any>(url, dto)
      .pipe(catchError(this.errorHandler));
  }


  // Confirm if user is logged in
  isLoggedIn() {
    return !!localStorage.getItem(AuthService.userDetailsToken);
  }

  // Get Token value
  getToken() {
    return localStorage.getItem(AuthService.userDetailsToken);
  }

  // Get Authorization Token
  getAuthToken(): string {
    if (this.getToken() == null) {
      return 'Bearer ';
    } else {
      return 'Bearer ' + this.getToken();
    }
  }

  // after login, set userdetails
  public setUserDetails(data: any): void {
    localStorage.setItem(AuthService.userDetailsEmail, data.email);
    localStorage.setItem(AuthService.userDetailsFullName, data.fullName);
    localStorage.setItem(AuthService.userDetailsToken, data.token);
    localStorage.setItem(AuthService.userDetailsRole, data.role);
  }

  // reset user details
  public resetUserDetails(): void {
    localStorage.removeItem(AuthService.userDetailsEmail);
    localStorage.removeItem(AuthService.userDetailsFullName);
    localStorage.removeItem(AuthService.userDetailsToken);
    localStorage.removeItem(AuthService.userDetailsRole);
  }

  public getUserDetailItem(userDetailItem: string) {
    const userDetail = localStorage.getItem(userDetailItem);
    if (!GeneralUtil.isValidString(userDetail)) {
      return null;
    }
    return userDetail;
  }

  // Error Handler
  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }


}
