import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {GeneralEnum} from "../constant/general-enum.enum";
import {catchError} from "rxjs/operators";
import {TransferFundDto} from "../dto/transfer-fund-dto";

@Injectable({
  providedIn: 'root'
})
export class WalletService {

  constructor(private httpClient: HttpClient) {
  }

  fundWallet(dto: any): Observable<any> {

    const url = GeneralEnum.baseUrl + '/auth/user/fund-wallet';

    return this.httpClient.post<any>(url, dto)
      .pipe(catchError(this.errorHandler));
  }


  transferFunds(dto: TransferFundDto): Observable<any> {

    const url = GeneralEnum.baseUrl + '/auth/user/wallet/credit-second-party';

    return this.httpClient.post<any>(url, dto)
      .pipe(catchError(this.errorHandler));
  }

  getWalletBalance(): Observable<any> {

    const url = GeneralEnum.baseUrl + '/auth/user/wallet-balance';

    return this.httpClient.get<any>(url)
      .pipe(catchError(this.errorHandler));
  }

  getWalletTransactions(): Observable<any> {

    const url = GeneralEnum.baseUrl + '/auth/user/wallet-transactions';

    return this.httpClient.get<any>(url)
      .pipe(catchError(this.errorHandler));
  }


  // Error Handler
  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }
}
