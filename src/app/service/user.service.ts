import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {LoginDto} from "../dto/login-dto";
import {Observable, throwError} from "rxjs";
import {GeneralEnum} from "../constant/general-enum.enum";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) {
  }

  getAllUsers(): Observable<any> {

    const url = GeneralEnum.baseUrl + '/auth/users';

    return this.httpClient.get<any>(url)
      .pipe(catchError(this.errorHandler));
  }


  // Error Handler
  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }
}
