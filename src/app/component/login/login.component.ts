import { Component, OnInit } from '@angular/core';
import {LoginDto} from "../../dto/login-dto";
import {AuthService} from "../../service/auth/auth.service";
import {Router} from "@angular/router";
import {GeneralUtil} from "../../util/general-util";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // Variables
  public loginDto = new LoginDto();
  public showPreloader = false;

  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.redirectUser();
  }



  // Login action
  loginAction() {

    this.showPreloader = true;
    this.authService.loginUser(this.loginDto)
      .subscribe(data => {
          this.showPreloader = false;
          this.authService.resetUserDetails();
          this.authService.setUserDetails(data);
          this.redirectUser();
        },
        error => {
          this.showPreloader = false;
          this.errorHandler(error);
        });
  }


  // redirect user after login
  redirectUser() {
    const userRole = this.authService.getUserDetailItem(AuthService.userDetailsRole);
    if (GeneralUtil.isValidString(userRole))
    {
      switch (userRole) {
        case 'PORTAL_USER' :
          this.router.navigate(['/dashboard']);
          return;

        default:
          return;

      }
    }
  }


  // Error handling
  public errorHandler(error: any) {

    console.log('error', error);
    if (error instanceof HttpErrorResponse) {
      if (error.status === 401) {
        //this.router.navigate(['/login']);
      }

      if (GeneralUtil.isValidString(error.error.message)) {
        alert(error.error.message);
        return;
      }

      if (GeneralUtil.isValidString(error.error.error)) {
        alert(error.error.error);
        return;
      }

      alert('Network error(1)');
      return;
    }
    alert('Network error(2)');
    return;
  }
}
