import { Component, OnInit } from '@angular/core';
import {RegisterDto} from "../../dto/register-dto";
import {AuthService} from "../../service/auth/auth.service";
import {Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {GeneralUtil} from "../../util/general-util";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  // variables
  public showPreloader = false;
  public registerUser = new RegisterDto();

  constructor(public authService: AuthService,
              public router: Router) { }

  ngOnInit(): void {
  }


  registerUserAction() {
    this.showPreloader = true;

    this.authService.registerUser(this.registerUser)
      .subscribe(data => {
        this.showPreloader = false;
        alert('User successfully registered');
        this.router.navigate(['/login']);
        return;
      },
        error => {
          this.showPreloader = false;
          this.errorHandler(error);
        })
  }


  // Error handling
  public errorHandler(error: any) {

    console.log('error', error);
    if (error instanceof HttpErrorResponse) {
      if (error.status === 401) {
        //this.router.navigate(['/login']);
      }

      if (GeneralUtil.isValidString(error.error.message)) {
        alert(error.error.message);
        return;
      }

      if (GeneralUtil.isValidString(error.error.error)) {
        alert(error.error.error);
        return;
      }

      alert('Network error(1)');
      return;
    }
    alert('Network error(2)');
    return;
  }

}
