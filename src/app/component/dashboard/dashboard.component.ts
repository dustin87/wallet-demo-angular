import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../../service/auth/auth.service";
import {WalletService} from "../../service/wallet.service";
import {HttpErrorResponse} from "@angular/common/http";
import {GeneralUtil} from "../../util/general-util";
import {Router} from "@angular/router";
import {UserDto} from "../../dto/user-dto";
import {TransferFundDto} from "../../dto/transfer-fund-dto";
import {UserService} from "../../service/user.service";
import {TransactionsDto} from "../../dto/transactions-dto";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  // variables
  public walletBalance = 0;
  public showPreloader = false;
  public registeredUsers: UserDto[] = [];
  public transactions: TransactionsDto[] = [];


  public fundWalletModal = false;
  public fundWalletPreloader = false;
  public fundWalletAmount = 0;
  // @ts-ignore
  @ViewChild('closeFundWalletModal') closeFundWalletModal: ElementRef;


  public tranferFundModal = false;
  public tranferFundPreloader = false;
  // @ts-ignore
  @ViewChild('closeTransferFundsModal') closeTransferFundsModal: ElementRef;
  public transferFundsDto = new TransferFundDto();




  constructor(public authService: AuthService,
              public router: Router,
              public userService: UserService,
              public walletService: WalletService) { }

  ngOnInit(): void {
    this.getWalletBalance();

    setTimeout(() => {
      this.getAllRegisterUsers();
    }, 500);

    setTimeout(() => {
      this.getTransactions();
    }, 500);
  }

  getAllRegisterUsers() {
    this.userService.getAllUsers()
      .subscribe(data =>  {

        this.registeredUsers = [];

        if (data.length > 0) {
          data.forEach((s: any) => {
            const user = new UserDto();
            user.mapToUserDto(s);
            this.registeredUsers.push(user);
          })
        }
      }, error => this.errorHandler(error));
  }

  getWalletBalance() {
    this.walletService.getWalletBalance()
      .subscribe(data => {
        this.walletBalance = data.amount;
      }, error => this.errorHandler(error));
  }


  fundWallet() {

    if (this.fundWalletAmount < 1) {
      alert('Amount must be greater than 0');
      return;
    }


    this.fundWalletPreloader = true;


    // prepare payload
    const payload = {
      amount: this.fundWalletAmount
    };

    this.walletService.fundWallet(payload)
      .subscribe(data => {
        alert('Funding successful');
        this.fundWalletPreloader = false;
        this.fundWalletAmount = 0;
        this.closeFundWalletModal.nativeElement.click();
        this.getWalletBalance();

        setTimeout(() => {
          this.getTransactions();
        }, 500);

      }, error => {
        this.fundWalletPreloader = false;
        this.errorHandler(error);
      })
  }

  transferFunds() {

    if (this.transferFundsDto.amount < 1) {
      alert('Amount must be greater than 0');
      return;
    }

    this.tranferFundPreloader = true;

    this.walletService.transferFunds(this.transferFundsDto)
      .subscribe(data => {

        this.tranferFundPreloader = false;
        alert('Transfer successful');
        this.transferFundsDto = new TransferFundDto();
        this.closeTransferFundsModal.nativeElement.click();
        this.getWalletBalance();

        setTimeout(() => {
          this.getTransactions();
        }, 500);

      }, error => {
        this.tranferFundPreloader = false;
        this.errorHandler(error);
      })

  }


  getTransactions() {
    this.walletService.getWalletTransactions()
      .subscribe(data => {

        this.transactions = [];

        if (data.length > 0) {

          // @ts-ignore
          data.forEach(t => {
            const trans = new TransactionsDto();
            trans.mapToTransactionDto(t);
            this.transactions.push(trans);
          })
        }


      }, error => this.errorHandler(error));
  }



  // Error handling
  public errorHandler(error: any) {

    console.log('error', error);
    if (error instanceof HttpErrorResponse) {
      if (error.status === 401) {
        this.router.navigate(['/login']);
      }

      if (GeneralUtil.isValidString(error.error.message)) {
        alert(error.error.message);
        return;
      }

      if (GeneralUtil.isValidString(error.error.error)) {
        alert(error.error.error);
        return;
      }

      alert('Network error(1)');
      return;
    }
    alert('Network error(2)');
    return;
  }
}
