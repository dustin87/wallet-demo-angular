import {GeneralUtil} from "../util/general-util";

export class UserDto {
  public id: number | undefined;
  public fullName: string | undefined;
  public email : string | undefined;
  public role: string | undefined;

  public constructor() {};

  public mapToUserDto(data: any):void {
    if (GeneralUtil.isValidJSON(data)) {
      this.id = data.id;
      this.fullName = data.fullName;
      this.email = data.email;
      this.role = data.role;
    }
  }
}
