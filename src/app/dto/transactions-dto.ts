import {GeneralUtil} from "../util/general-util";

export class TransactionsDto {
  // @ts-ignore
  public id: number;
  // @ts-ignore
  public transactionType: string;
  // @ts-ignore
  public amount: number;
  // @ts-ignore
  public transactionDescription: string;


  public mapToTransactionDto(data: any) {
    if (GeneralUtil.isValidJSON(data)) {
      this.id = data.id;
      this.transactionType = data.transactionType;
      this.amount = data.amount;
      this.transactionDescription = data.transactionDescription;
    }
  }
}
